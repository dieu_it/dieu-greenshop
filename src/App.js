import React from 'react';
import './App.css';
import Header from './component/layout/header';
import Footer from './component/layout/footer';
function App(props) {
  return (
    <div>
      <Header />
      {props.children}
      <Footer />
    </div>
  );
}

export default App;
