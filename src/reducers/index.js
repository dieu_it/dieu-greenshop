import { combineReducers } from 'redux';
import userReducer from './user';
import addToCart from './cart';

const rootReducer = combineReducers ({
    product : addToCart,
    user: userReducer
})
export default rootReducer;