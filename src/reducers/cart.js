const initialState = {
    cart : [
        {
            id : 1,
            name : "Cây chân chim",
            image : "../img/product/spx2-4.png",
            sale : "NEW",
            price : 500000
        },
        {
            id : 2,
            name : "Cây da lam",
            image : "../img/product/spx2-5.png",
            sale: "-50%",
            price : 850000,
        },
        {
            id : 3,
            name : "Cây danh dự",
            image : "../img/product/spx2-15.png",
            sale : "-20%",
            price : 150000
        }
    ],
    listCart : []
};
const addToCart = (state = initialState, action) => {
    console.log(action)
    switch (action.type) {
        case 'ADD_TO_CART' : {
            let updateCart = [...state.listCart]
            let index = updateCart.findIndex(item => item.id === action.payload.id);
            if(index !== -1) {
                updateCart[index].qty +=1;
            }
            else {
                updateCart.push(action.payload)
            }
            state.listCart = updateCart;
            return {...state}
        }
        case 'INCREMENT' : {
            let updateCart = [...state.listCart]
            let index = updateCart.findIndex(item =>item.id === action.payload.id);
            updateCart[index].qty +=1;
            state.listCart = updateCart;
            return {...state}
        }
        case 'DECREMENT' : {
            let updateCart = [...state.listCart]
            let index = updateCart.findIndex(item => item.id === action.payload.id);
            updateCart[index].qty -= 1 ;
            state.listCart = updateCart;
            return {...state};
        }
        case 'DELETE' : {
            let fil = state.listCart.filter(item => item.id !== action.payload.id)
            return {
                ...state,
                listCart: fil
            }
        }
        default:
            return state;
    }
}
export default addToCart;