import React from "react";
import Banner from "../content/banner";
import ProductHot1 from "../content/ProductHot1";
import ProductHot2 from "../content/productHot2";
import ProductHot3 from "../content/productHot3";
import ProductBuyHot from "../content/productBuyHot";
import ProductPromotion from "../content/productPromotion";
import ProductNew from "../content/productNew";
import ProductNews1 from "../content/productNews1";
import ProductNews2 from "../content/productNews2";
function home(props) {
    return (
      <React.Fragment>
        <Banner />
        <div className="product__hot">
          <div className="container">
            <h2 className="title">
              <a href="/">Sản phẩm nổi bật</a>
            </h2>
            <div className="row mt-5">
              <div className="col-sm-6 col-md-6 col-lg-6">
                <div className="row">
                  <div className="col-12">
                    <ProductHot1 />
                  </div>
                </div>
                <div className="row pt-3">
                  <div className="col-sm-6 col-md-6 col-lg-6">
                    <ProductHot2 />
                  </div>
                  <div className="col-sm-6 col-md-6 col-lg-6">
                    <ProductHot3 />
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-md-6 col-lg-6">
                <div className="row ">
                  <div className="col-sm-6 col-md-6 col-lg-6">
                    <ProductHot2 />
                  </div>
                  <div className="col-sm-6 col-md-6 col-lg-6">
                    <ProductHot3 />
                  </div>
                </div>
                <div className="row pt-3">
                  <div className="col-12">
                    <ProductHot1 />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="product__buyhot-promotion">
          <div className="container">
            <div className="row mt-5">
              <div className="col-sm-12 col-md-12 col-lg-3">
                <h2 className="title">
                  <a>Sản phẩm mua nhiều</a>
                </h2>
                <ul className="list-item">
                  <li>
                    <a>
                      <ProductBuyHot />
                    </a>
                  </li>
                  <li>
                    <a>
                      <ProductBuyHot />
                    </a>
                  </li>
                  <li>
                    <a>
                      <ProductBuyHot />
                    </a>
                  </li>
                  <li>
                    <a>
                      <ProductBuyHot />
                    </a>
                  </li>
                  <li>
                    <a>
                      <ProductBuyHot />
                    </a>
                  </li>
                  <li>
                    <a>
                      <ProductBuyHot />
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-sm-12 col-md-12 col-lg-9">
                <h2 className="title">
                  <a>Sản phẩm khuyến mãi</a>
                </h2>
                <ProductPromotion />
              </div>
            </div>
          </div>
        </div>
        <div className="product__banner">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <img src="../img/banner-review.png" alt="Banner Review" />
              </div>
            </div>
          </div>
        </div>
        <div className="product__new">
          <div className="container">
            <h2 className="title">
              <a>Sản phẩm mới</a>
            </h2>
            <ProductNew />
          </div>
        </div>
        <div className="product__news">
          <div className="container">
            <h2 className="title">
              <a>Tin tức</a>
            </h2>
            <div className="row mt-4">
              <div className="col-sm-12 col-md-4 col-lg-4">
                <ProductNews1 />
              </div>
              <div className="col-sm-12 col-md-4 col-lg-4">
                <ProductNews2 />
              </div>
              <div className="col-sm-12 col-md-4 col-lg-4">
                <ProductNews1 />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  
  
}

export default home;
