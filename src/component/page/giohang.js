import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import { addCart } from "../../actions/Cart";

function Giohang(props) {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.product.listCart);
  const handleClickCart = (item, index) => {
    const newProduct = { ...item, qty: 1 };
    const action = addCart(newProduct);
    dispatch(action);
  };
  // console.log(cart);
        return cart.map((item, index) => (
          <div className="product__giohang">
            <div className="container">
              <div className="title">
                <h3>
                  Home / <span>Giỏ hàng</span>
                </h3>
              </div>
              <div className="name">
                <h2>Giỏ hàng</h2>
              </div>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th scope="col">HÌNH ẢNH</th>
                    <th scope="col">TÊN SẢN PHẨM</th>
                    <th scope="col">ĐƠN GIÁ</th>
                    <th scope="col">SỐ LƯỢNG</th>
                    <th scope="col">THÀNH TIỀN</th>
                    <th scope="col">XÓA</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <img src={item.image} />
                    </td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  
                </tbody>
              </table>
              <div className="btn-chose">
                <button className="cancel">
                  <a>HỦY ĐƠN HÀNG</a>
                </button>
                <button className="active">
                  <a>TIẾP TỤC MUA</a>
                </button>
              </div>
              <div className="row mt-10">
                <div className="col-sm-0 col-md-0 col-lg-6"></div>
                <div className="col-sm-12 col-md-12 col-lg-6">
                  <table class="table table-bordered tong">
                    <tr>
                      <th className="th1">TỔNG TIỀN (CHƯA THUẾ)</th>
                      <td></td>
                    </tr>
                    <tr>
                      <th className="th2">THUẾ (VAT 10%)</th>
                      <td></td>
                    </tr>
                    <tr className="tr3">
                      <th>TỔNG PHẢI THANH TOÁN</th>
                      <td></td>
                    </tr>
                  </table>
                </div>
              </div>
              <div className="btn-chose">
                <button className="active">
                  <a>THANH TOÁN</a>
                </button>
              </div>
            </div>
          </div>
        ));
    
}

export default Giohang;