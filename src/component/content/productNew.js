import React, { Component } from "react";
import Slider from "react-slick";
import ProductHot1 from "../content/ProductHot1";
import ProductHot2 from "../content/productHot2";
import ProductHot3 from "../content/productHot3";
import ProductNew1 from "../content/productNew1";
import ProductNew2 from "../content/productNew2";
import ProductPromotion1 from "../content/productPromotion1";
import ProductPromotion2 from "../content/productPromotion2";
export default class SimpleSlider extends Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }
  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }
  render() {
    const settings = {
      dots: false,
      arrows: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    return (
      <div>
        <Slider ref={(a) => (this.slider = a)} {...settings}>
          <div>
            <div className="row mt-4">
              <div className="col-sm-12 col-md-3 col-lg-3">
                <ProductNew1 />
              </div>
              <div className="col-sm-12 col-md-3 col-lg-3">
                <ProductPromotion1 />
              </div>
              <div className="col-sm-12 col-md-3 col-lg-3">
                <ProductHot3 />
              </div>
              <div className="col-sm-12 col-md-3 col-lg-3">
                <ProductPromotion2 />
              </div>
            </div>
            <div className="row mt-4">
              <div className="col-sm-12 col-md-3 col-lg-3">
                <ProductHot2 />
              </div>
              <div className="col-sm-12 col-md-3 col-lg-3">
                <ProductHot1 />
              </div>
              <div className="col-sm-12 col-md-3 col-lg-3">
                <ProductHot3 />
              </div>
              <div className="col-sm-12 col-md-3 col-lg-3">
                <ProductNew2 />
              </div>
            </div>
          </div>
        </Slider>
        <div className="btn_slide-prev-next">
          <button className="button" onClick={this.previous}>
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          </button>
          <button className="button" onClick={this.next}>
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    );
  }
}
