import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addCart } from "../../actions/Cart";
function ProductHot3(props) {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.product.cart);
  const handleClickCart = (item, index) => {
    const newProduct = { ...item, qty: 1 };
    const action = addCart(newProduct);
    dispatch(action);
  };
  return cart.map((item, index) => (
    <div>
      <div className="product" key={index}>
        <div className="product-overlay">
          <img src={item.image} alt="Picture Cây Danh du" />
          <div className="saleleft1">
            <span>{item.sale}</span>
          </div>
          <div className="overlay">
            <div className="text">
              <ul className="btn-overlay">
                <li>
                  <a
                    className="add-cart"
                    id={item.id}
                    onClick={() => handleClickCart(item, index)}
                  >
                    MUA NGAY
                  </a>
                </li>
                <li>
                  <a className="search" href="/">
                    <i class="fa fa-search"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="product-content">
          <h3 className="title">
            <a href="#">{item.name}</a>
          </h3>
          <ul className="rating">
            <li className="fa fa-star checked" />
            <li className="fa fa-star checked" />
            <li className="fa fa-star checked" />
            <li className="fa fa-star checked" />
            <li className="fa fa-star checked-notfound" />
          </ul>
          <div className="price">
            {new Intl.NumberFormat().format(item.price)} đ
            <span className="price-sale">250.000 đ</span>
          </div>
        </div>
      </div>
    </div>
  ));
}
export default ProductHot3;
