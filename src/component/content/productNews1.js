import React from "react";
function productNews1(props) {
    return (
        <div className="list-news">
            <img src="../img/news/news1.png" alt="News 1"/>
            <ul className="note">
                <li><span>Thứ 7, ngày 21, tháng 12, năm 2015</span></li>
                <li><h3>15 thiết kế phòng ngủ tuyệt đẹp làm vạn người mê</h3></li>
                <li><p>Cùng Sài Gòn Hoa tìm hiểu một vài xu hướng thiết kế sân vườn được ưa chuộng hiện nay nhé</p></li>
                <li><a href="/">Đọc tiếp</a></li>
            </ul>
        </div>
    );
}
export default productNews1;