import React from "react";
function productPromotion1(props) {
  return (
    <div className="product">
      <div className="product-overlay">
        <img src="../img/product/spx2-13.png" alt="Picture Cây chân chim" />
        <div className="saleleft2">
          <span>-10%</span>
        </div>
        <div className="overlay">
          <div className="text">
            <ul className="btn-overlay">
              <li>
                <a className="add-cart">MUA NGAY</a>
              </li>
              <li>
                <a className="search" href="/">
                  <i class="fa fa-search"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="product-content">
        <h3 className="title">
          <a href="#">Cây Danh Dự</a>
        </h3>
        <ul className="rating">
          <li className="fa fa-star checked" />
          <li className="fa fa-star checked" />
          <li className="fa fa-star checked" />
          <li className="fa fa-star checked" />
          <li className="fa fa-star checked-notfound" />
        </ul>
        <div className="price">
          680.000 đ<span className="price-sale">68.000 đ</span>
        </div>
      </div>
    </div>
  );
}
export default productPromotion1;
