import React from "react";
function productBuyHot (props) {
    return (
      <div className="d-flex">
        <div className="images">
          <img src="../img/product/spx2-7.png" />
        </div>
        <div className="product-content_side">
          <h3 className="title">
            <a href="#">Cây da trâu</a>
          </h3>
          <ul className="rating">
            <li className="fa fa-star checked" />
            <li className="fa fa-star checked" />
            <li className="fa fa-star checked" />
            <li className="fa fa-star checked" />
            <li className="fa fa-star checked-notfound" />
          </ul>
          <div className="price"> 150.000đ</div>
        </div>
      </div>
    );
}
export default productBuyHot;