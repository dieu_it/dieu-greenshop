import React from "react";
function productNews2(props) {
  return (
    <div className="list-news">
      <img src="../img/news/news2.png" alt="News 2" />
      <ul className="note">
        <li>
          <span>Thứ 7, ngày 31, tháng 12, năm 2015</span>
        </li>
        <li>
          <h3>Tạo kiểu Cảnh góc sân cho nhà phố Biệt thự</h3>
        </li>
        <li>
          <p>
            Khi nước từ ngoài ngo vào hay từ trong nhà đưa góc sân là điểm nhìn đầu tiên của chúng ta
          </p>
        </li>
        <li>
          <a href="/">Đọc tiếp</a>
        </li>
      </ul>
    </div>
  );
}
export default productNews2;
