import React from "react";
footer.propTypes = {};
function footer(props) {
    return (
      <section className="footer">
        <div className="footer__top">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 col-md-12 col-lg-4">
                <p>KÊNH THÔNG TIN TỪ CHÚNG TÔI:</p>
                <ul className="connect">
                  <li>
                    <a>
                      <i className="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li>
                    <a>
                      <i className="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li>
                    <a>
                      <i className="fa fa-youtube-play" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li>
                    <a>
                      <i className="fa fa-vimeo" aria-hidden="true"></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-sm-12 col-md-12 col-lg-8">
                <div className="row">
                  <div className="col-sm-12 col-md-12 col-lg-4">
                    <p>ĐĂNG KÝ NHẬN EMAIL TỪ CHÚNG TÔI</p>
                  </div>
                  <div className="col-lg-8">
                    <div className="input-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder=""
                      />
                      <div className="input-group-prepend">
                        <button
                          className="btn btn-send"
                          type="button"
                          id="btn-send"
                        >
                          <i className="fa fa-paper-plane"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer__second">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 col-md-4 col-lg-4">
                <ul className="logo">
                  <li>
                    <a>
                      <img src="../img/logo.png" alt="Logo Green Shop" />
                    </a>
                  </li>
                  <li>
                    <a>
                      Green shop được thành lập từ 8/2010 được sự tin tưởng của
                      khách hàng trong suốt thời gian hoạt động đến nay cửa hàng
                      ngày một phát triển
                    </a>
                  </li>
                  <li>
                    <a>
                      <i class="fa fa-mobile"></i> Điện thoại: (84-4) 66.558.868
                    </a>
                  </li>
                  <li>
                    <a>
                      <i class="fa fa-envelope"></i> Email: info@dkt.com.vn
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-sm-12 col-md-12 col-lg-8">
                <div className="row">
                  <div className="col-sm-12 col-md-6 col-lg-4 mt-4">
                    <h5>THÔNG TIN KHÁCH HÀNG</h5>
                    <ul>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Tài khoản của tôi
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Sản phẩm yêu thích
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Lịch sử mua hàng
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Chính sách đổi trả
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Góp ý khiếu nại
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-sm-12 col-md-6 col-lg-3 mt-4">
                    <h5>HỖ TRỢ DỊCH VỤ</h5>
                    <ul>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Hệ thống cửa hàng
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Hướng dẫn mua hàng
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Hướng dẫn thanh toán
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Tích điểm đổi quà
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Câu hỏi thường gặp
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-sm-12 col-md-6 col-lg-3 mt-4">
                    <h5>CHÍNH SÁCH ƯU ĐÃI</h5>
                    <ul>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Chính sách giao hàng
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Chính sách đổi trả sản phẩm
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Chính sách bảo hành
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Giới thiệu sản phẩm mới
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Chính sách trả góp
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-sm-12 col-md-6 col-lg-2 mt-4">
                    <h5>TIN TỨC</h5>
                    <ul>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Tin mới
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Khuyến mãi
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Tuyển dụng
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Download
                        </a>
                      </li>
                      <li>
                        <a>
                          <i class="fa fa-chevron-right" aria-hidden="true"></i>
                          Tags
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer__bottom">
          <div className="container">
            <div className="row">
              <div className="col-sm-8 col-md-9 col-lg-9">
                <ul className="list-menu list-unstyled">
                  <li>
                    <a>Sitemap</a>
                  </li>
                  <li>
                    <a>Danh mục sản phẩm</a>
                  </li>
                  <li>
                    <a>Hợp tác</a>
                  </li>
                  <li>
                    <a>Thông tin liên hệ</a>
                  </li>
                  <li>
                    <a>Câu hỏi thường gặp</a>
                  </li>
                </ul>
              </div>
              <div className="col-sm-4 col-md-3 col-lg-3">
                <div className="payment_img">
                  <img src="../img/payment-methods.png" alt="image payment" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="copyright">
          <ul className="name-design">
            <a>
              Thiết kế bởi Ngọc Diệu
              <i class="fa fa-copyright" aria-hidden="true"></i>
            </a>
          </ul>
        </div>
      </section>
    );
}
export default footer;