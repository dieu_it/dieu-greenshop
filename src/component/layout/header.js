import React from "react";
import {Link} from 'react-router-dom'
function header(props){
    return (
      <section className="header">
        <div className="header__top">
          <div className="container">
            <div className="row">
              <div className="col-6 info-clock">
                <span>
                  <i class="fa fa-clock-o" aria-hidden="true"></i> Open time:
                  8:00 - 18:00 Monday - Sunday
                </span>
                <ul className="list-icon">
                  <li>
                    <a>
                      <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li>
                    <a>
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li>
                    <a>
                      <i class="fa fa-tumblr" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li>
                    <a>
                      <i class="fa fa-vimeo" aria-hidden="true"></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-6 info-log">
                <ul className="login-user">
                  <li>
                    <a>
                      <i class="fa fa-user" aria-hidden="true"></i> Đăng nhập
                    </a>
                  </li>
                  <li>
                    <a>
                      <i class="fa fa-user-plus" aria-hidden="true"></i> Đăng ký
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="header__second">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 col-md-12 col-lg-6 ">
                <img src="../img/logo.png" className="green-logo" />
                <img src="../img/beside_logo.png" className="beside-logo" />
              </div>
              <div className="col-4 search">
                <div className="phone ml-3 mb-3">
                  <a>
                    <i class="fa fa-phone" aria-hidden="true"></i> HỖ TRỢ: (04)
                    6674 2332 - (04) 3786 8904
                  </a>
                </div>
                <div className="input-group">
                  <input
                    type="text"
                    class="form-control search"
                    placeholder="Tìm kiếm..."
                  />
                  <div className="input-group-prepend">
                    <button
                      class="btn btn-search-posi"
                      type="button"
                      id="button-addon1"
                    >
                      <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-2 cart">
                <Link to="/giohang" style={{ textDecoration: 'none'}}>
                  <span>
                    <i class="fa fa-shopping-basket"></i> 0 Sản phẩm
                  </span>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="header__bottom">
          <div className="container">
            <nav className="navbar navbar-expand-lg navbar-light">
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon">
                  <i class="fa fa-bars" aria-hidden="true"></i>
                </span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav mr-auto">
                  <li className="nav-item active">
                    <Link className="nav-link" to="/" style={{color: "white"}}>
                      TRANG CHỦ <span className="sr-only">(current)</span>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      GIỚI THIỆU{" "}
                    </a>
                  </li>
                  <li className="nav-item dropdown">
                    <Link
                      to="/danhsachsanpham"
                      className="nav-link dropdown-toggle"
                      id="navbarDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      SẢN PHẨM
                    </Link>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdown"
                    >
                      <Link
                        to="/danhsachsanpham"
                        className="dropdown-item"
                        href="#"
                      >
                        Danh sach san pham
                      </Link>
                      <a className="dropdown-item" href="#">
                        Hoa chi do
                      </a>
                    </div>
                  </li>
                  <li className="nav-item dropdown">
                    <a
                      className="nav-link dropdown-toggle"
                      href="#"
                      id="navbarDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      SẢN PHẨM MỚI
                    </a>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdown"
                    >
                      <a className="dropdown-item" href="#">
                        Hoa chi do
                      </a>
                      <a className="dropdown-item" href="#">
                        Hoa chi do
                      </a>
                    </div>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      TIN TỨC
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      LIÊN HỆ
                    </a>
                  </li>
                </ul>
              </div>
              <ul className="icon-child">
                <li>
                  <i class="fa fa-search" aria-hidden="true"></i>
                </li>
                <li>
                  <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </section>
    );
}
export default header;