import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
// import Switch from 'react-bootstrap/esm/Switch';
import Home from './component/page/home';
import Danhsachsanpham from './component/page/danhsachsanpham';
import Giohang from './component/page/giohang';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import store from './store';
import {Provider } from 'react-redux';
ReactDOM.render(
  <div>
    <Provider store={store}>
      <Router>
        <App>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/danhsachsanpham" component={Danhsachsanpham} />
            <Route path="/giohang" component={Giohang} />
          </Switch>
        </App>
      </Router>
    </Provider>
  </div>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
